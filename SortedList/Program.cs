﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortedList
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var allwords = Console.ReadLine();
            Console.WriteLine();


            var sortedwords = Words.takeWords(allwords);
            foreach (KeyValuePair<string, string> word in sortedwords)
            {
                Console.WriteLine(word);
            }

            Console.WriteLine();

            Console.WriteLine("After supposed sorting");

            var newsortedwords = sortedwords.Shuffle();


            foreach (KeyValuePair<string, string> word in newsortedwords)
            {
                Console.WriteLine(word);
            }
        }


        //public static SortedList<string, string> Shuffle(string s)
        //{

        //    var sortedwords = Words.takeWords(s);


        //    var counter = 0;
        //    var listCount = sortedwords.Count - 1;
        //    var newSort = new SortedList<string, string>();
        //    string keyReplacer = "0";
        //    string[] sValues = s.Split();



        //    var queryWay = from sKeys in sValues
        //                   where sortedwords.ContainsValue(sKeys)
        //                   select sKeys;


        //    try
        //    {
        //        foreach (var word in queryWay)
        //        {
        //            newSort.Add(sortedwords[word], Helpers.assistList(sortedwords)[listCount]);
        //            listCount--;
        //            //counter++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"{ex.Message}. Bypassing the error");
        //        newSort.Add(Helpers.keyreplacer(keyReplacer, counter), Helpers.assistList(sortedwords)[listCount]);
        //        counter++;

        //    }

        //    return newSort;
        //}



    }


    public class Words
    {
        static SortedList<string, string> sortedWords = new SortedList<string, string>();


        public static SortedList<string, string> takeWords (string input)
        {

            string[] strArray = input.Split();

            int counter = 0;
            string keyReplacer = "0";

            while (counter < input.Split().Length)
            {

                try
                {
                    sortedWords.Add(strArray[counter], input.Split()[counter]);

                }catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message}. Bypassing the error");
                    sortedWords.Add(Helpers.keyreplacer(keyReplacer, counter), input.Split()[counter]);

                }

                counter++;
            }
            

            return sortedWords;

        }


    }

    public static class shuffler
    {
        public static SortedList<string, string> Shuffle(this SortedList<string, string> s)
        {
            var counter = 0;
            var listCount = s.Count - 1;
            var newSort = new SortedList<string, string>();
            string keyReplacer = "0";

            try
            {
                foreach (KeyValuePair<string, string> word in s)
                {
                    newSort.Add(s[word.Key], Helpers.assistList(s)[listCount]);
                    listCount--;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}. Bypassing the error");
                newSort.Add(Helpers.keyreplacer(keyReplacer, counter), Helpers.assistList(s)[listCount]);
            }

            return newSort;
        }


    }
}
