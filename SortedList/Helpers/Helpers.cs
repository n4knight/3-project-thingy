﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedList
{
    public static class Helpers
    {
        public static string keyreplacer(string replacer, int assist)
        {
            assist++;
            replacer = assist.ToString();

            return replacer;
        }

         public static List<string> assistList(SortedList<string, string> s)
        {
            var newList = new List<string>();

            foreach (KeyValuePair<string, string> word in s)
            {
                newList.Add(word.Value);

            }

            return newList;
        }
    }
}
