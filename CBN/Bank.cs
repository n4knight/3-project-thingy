﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBN
{
    class Bank
    {
        public event EventHandler<Fraud> fraudHandler;
        public void RaiseFraudAlert(string name)
        {
            Console.WriteLine("Fraud Alert!!!");
            fraudHandler?.Invoke(this, new Fraud(name));
        }


        private Dictionary<string, List<User>> _users = new Dictionary<string, List<User>>();
        private IList<string> _frauds = new List<string> { "jane", "james", "bill" };

        public Dictionary<string, List<User>> Users
        {
            get => _users;
        }

        public IList<string> Frauds
        {
            get => _frauds;
        }


        public void Add (User entity)
        {
            _users.Add(entity.Name, new List<User> 
            { new User { Name = entity.Name, Password = entity.Password, Bvn="" } });
            Console.WriteLine($"{entity.Name} your registration was successful");
            Console.WriteLine();
        }

        public bool Login (string name)
        {
            return _users.ContainsKey(name);
        }

        public void CheckBVN (User entity)
        {

            var _user = GetUser(entity.Name);


            if (!_users.ContainsKey(entity.Name))
            {
                Console.WriteLine($"No record of {entity.Name} found");
                return;
            }


            if (string.IsNullOrWhiteSpace(_user.Bvn))
            {
                Console.WriteLine("You don't have a BVN yet. Enroll from the Main Menu");
                return;
            }
            Console.WriteLine($"Your BVN: {_user.Bvn}");

            Console.WriteLine();
        }

        public void Enroll4BVN (User entity)
        {
            var _user = GetUser(entity.Name);

            if (string.IsNullOrWhiteSpace(_user.Bvn))
            {
                _user.Name = entity.Name;
                _user.Password = entity.Password;
                _user.Bvn = GenerateBVN();
                Console.WriteLine($"{_user.Name}, Your BVN: { _user.Bvn}");
                return;
            }

            Console.WriteLine("You can't enroll twice!!!");
            Console.WriteLine($"Your Bvn is {_user.Bvn}");
            Console.WriteLine();
        }

        public User GetUser (string name)
        {

            if (_users.ContainsKey(name)) return _users[name][0];
            return null;

        }

        string GenerateBVN()
        {
            int _loopLimit = 11, counter = 0;
            string bvn = "";

            while (counter < _loopLimit)
            {
                bvn += new Random().Next(_loopLimit - 1).ToString();
                counter++;
            }

            return bvn;
        }



    }



}
