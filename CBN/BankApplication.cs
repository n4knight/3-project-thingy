﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CBN.Helpers;

namespace CBN
{
    class BankApplication
    {
        private static readonly Bank _bank = new Bank();
        private static readonly StringBuilder sBuilder = new StringBuilder();


        public void Run ()
        {

            MainMenu:
                sBuilder.Clear();
                sBuilder.AppendLine("Welcome. \nPress");
                sBuilder.AppendLine("1. Register");
                sBuilder.AppendLine("2. Login");
                sBuilder.AppendLine("3. Check BVN");
                sBuilder.AppendLine("4. Enroll For BVN");
                sBuilder.AppendLine("5. Quit");


            var appRunning = true;


            while(appRunning)
            {
                Console.WriteLine(sBuilder.ToString());


                switch (Console.ReadLine())
                {
                    case "1":

                        Register();
                        DisplayPrompt();

                        if (Console.ReadLine() == "1") goto MainMenu;
                        else appRunning = false;

                        break;
                    case "2":

                        Login();
                        break;

                    case "3":
                        CheckBVN();
                        break;

                    case "4":
                        Enroll4BVN();
                        break;

                    case "5":
                        appRunning = false;
                        break;

                    default:
                        Console.WriteLine("Ivalid input. Try again");
                        goto MainMenu;
                }
            }
        }


        void Register()
        {
            Console.WriteLine("Enter your name");
            var name = Console.ReadLine(); 

            Console.WriteLine("Enter your password");
            var password = Console.ReadLine();

            while (Support.isNumber(name))
            {
                Console.Write("Name can't be empty or numbers. Try again ");
                name = Console.ReadLine();
            }

            while (string.IsNullOrWhiteSpace(password))
            {

                Console.Write("Password is required ");
                password = Console.ReadLine();
            }

            var data = new User { Name = name, Password = password, Bvn = "" };
            _bank.Add(data);

            Console.WriteLine();

        }

        void  Login ()
        {
            Console.WriteLine("Enter your name");
            var name = Console.ReadLine().ToLower();

            while (Support.isNumber(name))
            {
                Console.Write("Name can't be empty or number. Try again ");
                name = Console.ReadLine();
            }

            if (Support.isFraud(name))
            {
                var fraudReport = new CBN.Helpers.FraudReport();
                var fraud = new Fraud(name);
                _bank.fraudHandler += fraudReport.RaiseFraudReport;
                _bank.RaiseFraudAlert(name);
                return;
            }

            if (!_bank.Login(name))
            {
                Console.WriteLine($"no record of {name} found");
                return;
            }

            Console.WriteLine("Login successful");
            Console.WriteLine();
        }

        void CheckBVN ()
        {
            Console.WriteLine("Enter your name");
            var name = Console.ReadLine();

            Console.WriteLine("Enter your password");
            var password = Console.ReadLine();

            while (Support.isNumber(name))
            {
                Console.Write("Name can't be empty or numbers. Try again ");
                name = Console.ReadLine();
            }

            while (string.IsNullOrWhiteSpace(password))
            {

                Console.Write("Password is required ");
                password = Console.ReadLine();
            }

            var data = new User { Name = name, Password = password, Bvn = "" };
            _bank.CheckBVN(data);
        }

        void Enroll4BVN ()
        {
            Console.WriteLine("Enter your name");
            var name = Console.ReadLine();

            Console.WriteLine("Enter your password");
            var password = Console.ReadLine();

            while (Support.isNumber(name))
            {
                Console.Write("Name can't be empty or numbers. Try again ");
                name = Console.ReadLine();
            }

            while (string.IsNullOrWhiteSpace(password))
            {

                Console.Write("Password is required ");
                password = Console.ReadLine();
            }

            var data = new User { Name = name, Password = password, Bvn = "" };
            _bank.Enroll4BVN(data);
        }

        static void DisplayPrompt ()
        {
            Console.WriteLine("1. Main menu \n2. Exit");
        }
    }


}
