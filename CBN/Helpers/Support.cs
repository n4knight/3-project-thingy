﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBN
{
    public class Support
    {
        public static bool isFraud(string name)
        {
            Bank bankInstance = new Bank();

            return bankInstance.Frauds.Contains(name);
        }

        public static bool isNumber (string input)
        {
            return int.TryParse(input, out int result) || string.IsNullOrWhiteSpace(input);
        }
    }
}
