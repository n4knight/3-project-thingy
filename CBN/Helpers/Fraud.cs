﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBN
{
    public class Fraud:EventArgs
    {
        string _name;

        public Fraud (string name)
        {
            _name = name;
        }

        public string Name
        {
            get => _name;
        }
    }

}
