﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBN
{
    class Program
    {
        static void Main (string [] args)
        {
            BankApplication _bankApplication = new BankApplication();

            _bankApplication.Run();
        }
    }
}
