﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace intNChar
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        static string CharMatcher (string input)
        {

            var characterList = characters._intChar();
            string string2Return = "";
            char[] chars = input.ToCharArray();


            var allchars = from c in chars
                           where characterList.Contains(c)
                           select c;

            foreach (var validChar in allchars)
            {
                string2Return += characterList.IndexOf(validChar);
            }

            return string2Return;

        }

        public static string ValidCharacterExtractor(string inputString)
        {
            var characterList = characters._intChar();
            string chars2Return = "";
            char[] chars = inputString.ToCharArray();

            var result = chars.Where(c => characterList.Contains(c));

            foreach (var inputChar in result)
            {
                chars2Return += inputChar;
            }

            return chars2Return;
        }

        static bool CheckForWrongInput(string inputString)
        {
            var characterList = characters._intChar();
            char[] chars = inputString.ToCharArray();

            for (var i = 0; i < inputString.Length; i++)
            {
                if (characterList.Contains(chars[i]))
                    return true;
            }

            return false;
        }

        static void Run()
        {
            Begin:
                Console.WriteLine("Enter some text with special characters");
                var characters = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(characters))
            {
                goto Begin;
            }

            while (!CheckForWrongInput(characters))
            {
                goto Begin;
            }

            var result = CharMatcher(characters);
            Console.WriteLine($"{ValidCharacterExtractor(characters)} corresponds to : {result}");
        }
    }


    public static class characters
    {

        public static List<char> _intChar()
        {

            var charList = new List<char>(){ '!',')','(','*','&','^','%','$','#','@' };

            return charList;
        }

    }


}
