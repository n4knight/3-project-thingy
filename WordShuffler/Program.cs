﻿using System;
using System.Linq;

namespace WordShuffler
{
    class Program
    {
        static void Main(string[] args)
        {
            start:
                Console.WriteLine("Enter some text you would like to shuffle");
                string _input = Console.ReadLine();
            Console.WriteLine();

            while (string.IsNullOrWhiteSpace(_input))
            {
                goto start;
            }

            var _inputArray = _input.Split();

            var _randomNumber = new Random();

            var _methodQuery = _inputArray.OrderBy(shuffler => _randomNumber.Next());

            Console.WriteLine($"Using method syntax: {string.Join(" ", _methodQuery)}");

            Console.WriteLine();

            var _querySyntax = from word in _inputArray orderby _randomNumber.Next() select word;

            Console.WriteLine($"Using query Syntax: {string.Join(" ", _querySyntax)}");



        }
    }
}
